#Ender's Game Flash Gun Control

**Lead Developer:** _GlitchTech Science_

**Last major update:** 2014.10.07

**Version:** 1.0.0


About
-----

Flash Gun from the movie Ender's Game. This model is a fairly screen accurate version. It has space for electronics to light up the 'spinner' and the 'emitter' to simulate firing like in the movie. [Video](https://www.youtube.com/watch?v=7ljqAWrR-_A)


Version Control
---------------

This project is under version control using git: [GitLab Page](https://gitlab.com/glitchtechscience/enders-game-flash-gun).


Parts
-----

[3D Printable Models](http://www.thingiverse.com/thing:417286)

The following are suggested electronics. Some items, like the single LED and resistors, can be swapped out as long as you use suitable replacement items.

- [NeoPixel Ring - 12 x WS2812 5050 RGB LED with Integrated Drivers](http://www.adafruit.com/products/1643) - 2 units @ $7.50 each
- [Arduino Pro Mini 328 - 5V/16MHz](https://www.sparkfun.com/products/11113) - 1 unit @ $9.95 each
- [Polymer Lithium Ion Battery - 1000mAh](https://www.sparkfun.com/products/339) - 1 unit @ $8.95 each
- [Power Cell - LiPo Charger/Booster](https://www.sparkfun.com/products/11231) - 1 unit @ $19.95 each
- [SPST Sub-Mini Slide Switch](http://www.radioshack.com/product/index.jsp?productId=19964156) - 1 unit @ $3.49 each
- [Mini Push Button Switch](https://www.sparkfun.com/products/97) - 1 unit @ $0.35
- Push Button Resistor - 1/4W 10 Kohm Resistor
- [3mm White LED](https://www.superbrightleds.com/moreinfo/through-hole/white-3mm-led-45-degree-viewing-angle-6000-mcd/302/1234/#/tab/Specifications) - 1 unit @ $0.74
- LED Resistor - 1/2W 18 ohm resistor

I used 4-40 bolts to hold it together with hexagonal stand-offs (15.5mm long) in the gun body. Each on the surface has a standard washer. There is also one 4-40 bolt buried in the body connected to the battery sled. It uses a standard nut to connect things.

*I will be updating the design to use 4-40 screws instead of bolts. The stand-offs have become difficult to find.*
