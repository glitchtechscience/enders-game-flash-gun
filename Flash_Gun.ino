/*
 Flash Gun Control
 GlitchTech Science 2014
 */

/** Library Includes */

#include <Adafruit_NeoPixel.h>

/** Constants */

#define LED_COUNT 12

const int STS_PIN = 13;
const int DATA_PIN = 12;

const int BTN_PIN = 7;
const int LED_PIN = 9;

const unsigned long LED_COLOR = 0xffffff;

/** Variables */

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel circle_r = Adafruit_NeoPixel( LED_COUNT * 2, DATA_PIN, NEO_GRB + NEO_KHZ800 );

int dir = 25;
int fade = 0;
int buttonState = 0;

void setup() {

  // Trigger init
  pinMode( BTN_PIN, INPUT );

  // Initialize LED strip
  circle_r.begin();
  circle_r.show();
}

void loop() {

  buttonState = digitalRead( BTN_PIN );
  circle_r.setBrightness( 64 );

  if( buttonState == HIGH ) {

    if( fade < 255 ) {

      run( LED_COLOR, ( 255 - fade ) / 20 );

      // sets the value (range from 0 to 255):
      analogWrite( LED_PIN, fade );

      fade = fade + dir;
    } 
    else {

      analogWrite( LED_PIN, 255 );

      for( int i = 0; i < LED_COUNT * 2; i++ ) {

        circle_r.setPixelColor( i, LED_COLOR );
      }
      
      showLEDs();
    }
  } 
  else {

    clearLEDs();
    showLEDs();

    fade = 0;

    analogWrite( LED_PIN, fade );
  }
}

void clearLEDs() {

  for( int i = 0; i < LED_COUNT * 2; i++ ) {

    circle_r.setPixelColor( i, 0 );
  }
}

void showLEDs() {

  // LED on to signify sending
  digitalWrite( STS_PIN, HIGH );

  // Write
  circle_r.show();

  // LED off
  digitalWrite( STS_PIN, LOW );
}

////////////////////

/**
 * Cylon eye scanner. Runs one complete cycle.
 *
 * @param unsigned long  color  Color of 'eye'
 * @param bye  wait  Delay between 'steps'.
 */
void run( unsigned long color, byte wait ) {

  // weight determines how much lighter the outer "eye" colors are
  const byte weight = 12;  

  // It'll be easier to decrement each of these colors individually
  // so we'll split them out of the 24-bit color value
  byte red = ( color & 0xFF0000 ) >> 16;
  byte green = ( color & 0x00FF00 ) >> 8;
  byte blue = ( color & 0x0000FF );

  // Start at closest LED, and move to the outside
  for( int i = 0; i <= LED_COUNT - 1; i++ ) {

    led_slave( i, red, green, blue, weight );
    delay( wait );
  }
}

/**
 * Slave function to handle work for cylon.
 * 'Dims' the LEDs to the side of the 'eye'.
 */
void led_slave( int i, byte red, byte green, byte blue, byte weight ) {

  clearLEDs();

  // Set the bright middle eye
  circle_r.setPixelColor( i, red, green, blue );  
  circle_r.setPixelColor( ( i + LED_COUNT ), red, green, blue );  

  // Now set two eyes to each side to get progressively dimmer
  for( int j = 1; j < 3; j++ ) {

    if( i - j >= 0 ) {

      circle_r.setPixelColor( i - j, red / ( weight * j ), green / ( weight * j ), blue / ( weight * j ) );
      circle_r.setPixelColor( ( i + LED_COUNT ) - j, red / ( weight * j ), green / ( weight * j ), blue / ( weight * j ) );
    }

    if( i - j <= LED_COUNT ) {

      circle_r.setPixelColor( i + j, red / ( weight * j ), green / ( weight * j ), blue / ( weight * j ) );
      circle_r.setPixelColor( ( i + LED_COUNT ) + j, red / ( weight * j ), green / ( weight * j ), blue / ( weight * j ) );
    }
  }

  showLEDs();// Turn the LEDs on
}
